const config = require('./config')
      restify = require('restify'),
      lodash = require('lodash'),
      topicController = require('./http/routes/route'),
      plugins = require('restify-plugins'),
      mongoose = require('mongoose');


global.server = restify.createServer({
  name: config.name,
  version: config.version,
});

server.use(plugins.acceptParser(server.acceptable));
server.use(plugins.bodyParser());
server.use(plugins.queryParser());

topicController(server, require('./http/controllers/TopicController'));

server.on('uncaughtException', (req, res, route, error)=>{
  console.error(error.stack);
  res.send(error);
});

server.listen(config.port, ()=>{
  console.log('%s listing on %s', config.base_url, config.port);

mongoose.connection.on('error', function(err){
  console.log(err);
  process.exit(1);
});

  global.db = mongoose.connect(config.db.uri, {
    useMongoClient: true,
  });

});
