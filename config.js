module.exports = {

  name: 'Ghana Forum based application',
  version: '0.0.1',
  port: process.env.PORT || 3000,
  base_url: process.env.BASE_URL || 'http://localhost:3000',
  env: process.env.NODE_ENV || 'development',
  db: {
    uri: 'mongodb://root:pa33word@ds129013.mlab.com:29013/forum_gh_database',
  }


}
