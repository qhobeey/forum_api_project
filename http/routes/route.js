const Router = require('restify-router').Router,
      _ = require('lodash'),
      respond = require('./respond');

let registerRoutes = (server, controller)=>{

  let router = new Router();

  /* Topic Routes */

  router.get('', (req, res, next)=>{
    controller.getTopic().then(function(data){
      console.log(data);
      ! _.isEmpty(data)?respond.success(res, next, data):respond.failure(res, next, 'no data found', 400);
    }).catch(function(err){
      console.log(err);
      respond.failure(res, next, err, 400);
    });
  });

  router.get('/:id', (req, res, next)=>{
    let id = req.params.id;
    controller.getTopicById(id).then(function(data){
      console.log(data);
      ! _.isEmpty(data)?respond.success(res, next, data):respond.failure(res, next, 'no data found', 400);
    }).catch(function(err){
      console.log(err);
      respond.failure(res, next, err, 400);
    });

  });

  router.post('', (req, res, next)=>{
    controller.createTopic(req.params).then(function(data){
      data?respond.success(res, next, data):respond.failure(res, next, 'error saving', 400);
    });

  });

  router.applyRoutes(server, '/api/forums/topic');

}

module.exports = registerRoutes;
