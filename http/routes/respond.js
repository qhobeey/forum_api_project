function respond(res, next, status, data, http_code){

  let response = {
    'status': status,
    'data': data
  };

  res.writeHead(http_code, {'content-type': 'application/json'});
  res.end(JSON.stringify(response));
  next();

};

module.exports.success = (res, next, data)=>{
  return respond(res, next, 'sucess', data, 200);
}

module.exports.failure = (res, next, data, http_code)=>{
  return respond(res, next, 'failure', data, http_code);
}
