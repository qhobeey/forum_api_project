const _ = require('lodash'),
      mongoose = require('mongoose'),
      Topic = require('../../models/topicSchema');

class TopicController{

  /* Access all topics in databas*/
  getTopic(){
    return new Promise(function(resolve, reject){
      let tp = Topic;
      tp.find({}, function(err, topics){
        if(! _.isEmpty(topics)){
          resolve(topics);
        }else{
          reject('sorry no data found in database');
        }
      });
    });
  }


  /* Creating a new topic */
  createTopic(params){
    return new Promise(function(resolve, reject){
      let tp = new Topic();
      tp.title = params.title;
      tp.save(function(err, topic){
        if(err){
          reject(err);
        }
        if(topic){
          resolve(topic);
        }
      });
    });
  }


  /* Creating a new topic */
  getTopicById(id){
    return new Promise(function(resolve, reject){
      let tp = Topic;
      tp.findOne({_id:id}, {_id: false}, function(err, topic){
        if(err){
          reject(err);
        }
        resolve(topic);
      });
    });

  }






}
module.exports = new TopicController();
