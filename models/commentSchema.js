const mongoose = require('mongoose'),
      uuid = require('uuid/v4');

let Schema = mongoose.Schema;
let ObjectId = Schema.Types.ObjectId;

let commentSchema = new Schema({
  id: {type: String, default: uuid()},
  _topic: {type: Schema.Types.ObjectId, ref: 'topics'},
  content: {type: String, required:true, trim: true},
  created: {type: Date, default: Date.now},
  updated: {type: Date, default: Date.now},
});

let Comment = mongoose.model('comments', commentSchema);

module.exports = Comment;
