const mongoose = require('mongoose'),
      uuid = require('uuid/v4');

let Schema = mongoose.Schema;
let ObjectId = Schema.Types.ObjectId;

let topicSchema = new Schema({
  id: { type: String, default: uuid() },
  title: { type: String, required: true, trim: true },
  closed: { type: Boolean, default: 0 },
  created: { type: Date, default: Date.now, require: true},
  updated: { type: Date, default: Date.now, require: true},
});

let Topic = mongoose.model('topics', topicSchema);

module.exports = Topic;
